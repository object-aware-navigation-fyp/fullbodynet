import os

import torch
import torchvision.transforms as transforms
import numpy as np

from .full_body_net import FullBodyNet

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

class Encoder:
    def __init__(self, weights_path = ROOT_DIR + '/weights/mars_triplet_500.pth'):
        # use GPU if avail
        if torch.cuda.is_available():
            self.device = 'cuda'
        else:
            self.device = 'cpu'
        
        net = FullBodyNet()
        net.load_state_dict(torch.load(weights_path))
        net.to(self.device)
        net.eval()
        self.net = net

    # transform for nets trained on imagenet
    def imagenet_transform(self):
        return transforms.Compose([
            transforms.Resize((299, 299)),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ])

    def encode(self, images, batch_size=8):
        # transform
        transform = self.imagenet_transform()
        images = [transform(i) for i in images]

        # separate images into batches
        batches = []
        index = -1

        for i, image in enumerate(images):
            if i % batch_size == 0:
                batches.append([])
                index += 1
            
            batches[index].append(image)

        # get encodings
        encodings = []

        with torch.no_grad():
            for batch in batches:
                batch = torch.stack(batch).to(self.device)
                outputs = self.net(batch).cpu().numpy()

                for encoding in outputs:
                    encodings.append(encoding)
            
        return np.array(encodings)
